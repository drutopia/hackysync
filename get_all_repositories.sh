#! /bin/sh

while read project
do
  git clone git@gitlab.com:drutopia/$project.git
  cd $project
  git remote add drupal git@git.drupal.org:project/$project.git
  cd ..
done < drutopia_projects.txt

while read project
do
  git clone git@git.drupal.org:project/$project.git
done < drupal_org_only.txt

mkdir drutopia-infrastructure

while read project
do
  cd drutopia-infrastructure
  git clone git@gitlab.com:drutopia/$project.git
  cd $project
  git remote add drupal git@git.drupal.org:project/$project.git
  cd ../..
done < drutopia_infrastructure_projects.txt


mkdir findit
cd findit

while read project
do
  git clone git@gitlab.com:find-it-program-locator/$project.git
  cd $project
  git remote add drupal git@git.drupal.org:project/$project.git
  cd ../
done < ../findit_projects.txt

cd ../


mkdir -p agaric/drupal
cd agaric/drupal

while read project
do
  git clone git@gitlab.com:agaric/drupal/$project.git
  cd $project
  git remote add drupal git@git.drupal.org:project/$project.git
  cd ../
done < ../../agaric_projects.txt

cd ../../
