# List commits since most recent tag
# As a bonus, it's markdown formatted

for i in */.git; do ( echo "### ${i::-5}"; cd $i/..; git log `git describe --tags --abbrev=0`..HEAD --pretty=format:"[%h](https://gitlab.com/drutopia/${i::-5}/commit/%H) %s (%an)  "; ); done

# let's not worry about infrastructure projects for now
# for i in drutopia-infrastructure/*/.git; do ( echo $i; cd $i/..; ... ); done

# MANUAL APPROACH
#
# To do it for just one repository, after already tagging the release:
#
# git log 8.x-1.0-alpha2..8.x-1.0-alpha3 --pretty=format:"[%h](https://gitlab.com/drutopia/${PWD##*/}/commit/%H) %s (%an)  "
